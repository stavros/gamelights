#include <AsyncMqttClient.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WiFiManager.h>
#include <ESP8266mDNS.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>

#define RPIN 13
#define GPIN 12
#define BPIN 14
#define LED_PIN 2

#ifndef OTA_PASS
#define OTA_PASS "an ota pass"
#endif

#ifndef PROJECT_NAME
#define PROJECT_NAME "project"
#endif

#ifndef MQTT_SERVER
#define MQTT_SERVER "test.mosquitto.org"
#endif

#define MQTT_PORT 1883
#define UDP_PORT 19872

WiFiUDP Udp;
WiFiClient wclient;
char COMMAND_TOPIC[128];
char LOG_TOPIC[128];

char INSTANCE_NAME[16];

AsyncMqttClient mqttClient;


void color(int r, int g, int b) {
    analogWrite(RPIN, constrain(r, 0, 1023));
    analogWrite(GPIN, constrain(g, 0, 1023));
    analogWrite(BPIN, constrain(b, 0, 1023));
}


void onMqttConnect(bool sessionPresent) {
    Serial.println("Connected to MQTT.");
    mqttClient.subscribe(COMMAND_TOPIC, 2);
    mqttPublish(LOG_TOPIC, "Connected.");
}


void onMqttDisconnect(AsyncMqttClientDisconnectReason reason) {
    mqttClient.connect();
}


void onMqttMessage(char* topic, char* payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total) {
    payload[len] = '\0';
    parseCommand((char *)payload);
}


void mqttPublish(String topic, String payload) {
    char chPayload[500];
    char chTopic[100];

    if (!mqttClient.connected()) {
        return;
    }

    topic.toCharArray(chTopic, 100);
    ("(" + String(millis()) + " - " + WiFi.localIP().toString() + ") " + INSTANCE_NAME + ": " + payload).toCharArray(chPayload, 100);
    mqttClient.publish(chTopic, 0, false, chPayload, 0);
}


void connectMQTT() {
    Serial.println("Connecting to MQTT...");

    mqttClient.onConnect(onMqttConnect);
    mqttClient.onDisconnect(onMqttDisconnect);
    mqttClient.onMessage(onMqttMessage);

    mqttClient.setServer(MQTT_SERVER, MQTT_PORT);

    mqttClient.connect();
}


// Receive a message from MQTT and act on it.
void mqttCallback(char* chTopic, byte* chPayload, unsigned int length) {
    chPayload[length] = '\0';
    parseCommand((char *)chPayload);
}


void connectWifi() {
    WiFiManager wifiManager;
    char apName[64];

    WiFi.hostname((String("GLD_") + String(INSTANCE_NAME)).c_str());
    wifiManager.setConfigPortalTimeout(180);
    snprintf(apName, 64, PROJECT_NAME "-%08X", ESP.getChipId());
    if (!wifiManager.autoConnect(apName)) {
        Serial.println("Failed to connect.");
        ESP.reset();
        delay(5000);
    }

    Serial.println("Connected to WiFi. IP:");
    Serial.println(WiFi.localIP());
}


void parseCommand(char* command) {
    int r, g, b;
    int i = 0;
    char *saveptr;
    char* text = strtok_r(command, ", ", &saveptr);
    while (text != NULL) {
        switch (i) {
        case 0:
            r = atoi(text);
            break;
        case 1:
            g = atoi(text);
            break;
        case 2:
            b = atoi(text);
            break;
        default:
            return;
        }
        i++;
        text = strtok_r(NULL, ", ", &saveptr);
    }
    color(r, g, b);
}


void readUDP() {
    byte packetBuffer[512];
    int noBytes = Udp.parsePacket();

    if (noBytes <= 0) {
        return;
    }

    Udp.read(packetBuffer, noBytes);
    packetBuffer[noBytes] = '\0';

    parseCommand((char *)packetBuffer);
}


#ifdef OTA_SERVER
void doHTTPUpdate() {
    if (WiFi.status() != WL_CONNECTED) return;

    t_httpUpdate_return ret = ESPhttpUpdate.update("http://" OTA_SERVER "/" PROJECT_NAME "/", VERSION);

    switch(ret) {
        case HTTP_UPDATE_FAILED:
            Serial.printf("HTTP_UPDATE_FAILED Error (%d): %s\r\n", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
            break;

        case HTTP_UPDATE_NO_UPDATES:
            Serial.println("HTTP_UPDATE_NO_UPDATES");
            break;

        case HTTP_UPDATE_OK:
            Serial.println("HTTP_UPDATE_OK");
            break;
    }
}
#endif


void resetPins() {
    pinMode(1, OUTPUT);
    analogWrite(1, 0);
    pinMode(2, OUTPUT);
    analogWrite(2, 0);
    pinMode(3, OUTPUT);
    analogWrite(3, 0);
    pinMode(4, OUTPUT);
    analogWrite(4, 0);
    pinMode(5, OUTPUT);
    analogWrite(5, 0);
    pinMode(12, OUTPUT);
    analogWrite(12, 0);
    pinMode(13, OUTPUT);
    analogWrite(13, 0);
    pinMode(14, OUTPUT);
    analogWrite(14, 0);
    pinMode(15, OUTPUT);
    analogWrite(15, 0);
}


void setupmDNS() {
    char hostString[16] = {0};

    sprintf(hostString, "gamelights_%06X", ESP.getChipId());

    if (!MDNS.begin(hostString)) {
        Serial.println("Error setting up MDNS responder!");
    }

    MDNS.addService("gamelights", "udp", UDP_PORT);
}

void setup() {
    resetPins();
    pinMode(LED_PIN, OUTPUT);
    pinMode(RPIN, OUTPUT);
    pinMode(GPIN, OUTPUT);
    pinMode(BPIN, OUTPUT);

    Serial.begin(115200);
    Serial.println("Booting");

    setupmDNS();

    Serial.println("\n\nBooting version " VERSION "...");

    snprintf(INSTANCE_NAME, 16, "%08X", ESP.getChipId());
    snprintf(COMMAND_TOPIC, 128, PROJECT_NAME "/%08X/command", ESP.getChipId());
    snprintf(LOG_TOPIC, 128, PROJECT_NAME "/%08X/log", ESP.getChipId());

    connectWifi();
#ifdef OTA_SERVER
    doHTTPUpdate();
#endif
    connectMQTT();

    Udp.begin(UDP_PORT);

    // Hostname defaults to esp8266-[ChipID]
    ArduinoOTA.setHostname((String(PROJECT_NAME "-") + String(INSTANCE_NAME)).c_str());

    ArduinoOTA.onStart([]() {
        resetPins();
        Serial.println("Start");
    });
    ArduinoOTA.onEnd([]() {
        Serial.println("\nEnd");
    });
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    });
    ArduinoOTA.onError([](ota_error_t error) {
        Serial.printf("Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR) {
            Serial.println("Auth Failed");
        } else if (error == OTA_BEGIN_ERROR) {
            Serial.println("Begin Failed");
        } else if (error == OTA_CONNECT_ERROR) {
            Serial.println("Connect Failed");
        } else if (error == OTA_RECEIVE_ERROR) {
            Serial.println("Receive Failed");
        } else if (error == OTA_END_ERROR) {
            Serial.println("End Failed");
        }
    });
    ArduinoOTA.begin();
}

void flashLED() {
    // Blink for the first minute, just as a health test.
    if (millis() % 1000 > 100 || millis() > 60 * 1000) {
        digitalWrite(LED_PIN, HIGH);
    } else {
        digitalWrite(LED_PIN, LOW);
    }
}

// the loop function runs over and over again forever
void loop() {
    readUDP();
    flashLED();
    ArduinoOTA.handle();
}
